import axios from 'axios'

import logger from '../logger'

export const socketAuth = async(socket, next) => {
    const tokenAuth = socket.handshake.query.token

    if (!tokenAuth) {
        return next(new Error('User dont authorizated.'))
    }

    /**
    * Decode user token for auth service.
    *
    * @param {string} token
    * @return {id}
    */
    const apiUrl = `${ process.env.REQUEST_DECODE_TOKEN_HOST }/token/${ tokenAuth }`

    try {
        const { data } = await axios.get(apiUrl, {
            headers: {
                'authopenh': process.env.REQUEST_DECODE_TOKEN_HEADER_AUTH,
                'Content-type': 'application/json'
            }
        })

        socket.request.tokenParse = {
            userId: data.id
        }

        next()
    } catch (err) {
        logger.error(`${ err.status || 500 } - ${ err.message } - ${ apiUrl }`)

        next(new Error('Token incorrected.'))
    }
}
