import fs from 'fs'
import http from 'http'
import https from 'https'

const NODE_ENV = process.env.NODE_ENV

/**
 * Created server for env mode.
 * @param {*} app
 * @returns server
 */
export const createServer = (app) => {
    // Runnig server with SSL certificate (HTTPS / WSS)
    if (NODE_ENV === 'production') {
        return https.createServer({
            key: fs.readFileSync('../server.key'),
            cert: fs.readFileSync('../server.crt')
        }, app)
    }

    // Runnig server (HTTP / WS)
    return http.Server(app)
}
