import express from 'express'
import dotenv from 'dotenv'
import morgan from 'morgan'
import logger from './logger'
import { createServer } from './createServer'
import { socketAuth } from './middleware/socketAuth'

dotenv.config()

const app = express()
const server = createServer(app)
const io = require('socket.io')(server)
const Redis = require('ioredis')

const PORT = process.env.PORT

const optionsRedis = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASSWORD,
    showFriendlyErrorStack: true,
    autoResubscribe: false,
    retryStrategy: (times) => Math.min(times * 50, 2000)
}

app.use(morgan('combined', { stream: logger.stream }))

let activeUser = 0
const bell = io.of('/bell')

// Check auth token user
bell.use(socketAuth)

bell.on('connection', (socket) => {
    const { userId } = socket.request.tokenParse
    const subRedis = new Redis(optionsRedis)

    activeUser++

    logger.info(`Active users: ${ activeUser }`)

    // Auth user connected
    if (userId) {
        logger.info(`Active users: ${ activeUser }`)

        subRedis.subscribe(`bell:${ userId }`, () => {
            logger.info(`Subscribe to <bell:${ userId }>`)

            // Listener event for new notify
            subRedis.on('message', (channel, message) => {
                logger.debug(`${ channel } - ${ message }`)
                socket.emit('data', message)
            })
        })
    }

    // User disconnect
    socket.on('onClose', () => {
        socket.disconnect(true)
    })

    // Close connect with client
    socket.on('disconnect', () => {
        global.console.log('user disconnected')
        subRedis.unsubscribe(`bell:${ userId }`)
        subRedis.quit()
        activeUser--
    })
})

server.listen(PORT, (err) => {
    if (err) {
        logger.error(err.message)
        return global.console.log(err)
    }

    global.console.log(`Listening on *: ${ PORT }`)
})

module.exports = app
