import { createLogger, format, transports, config } from 'winston'
import { transportOptions } from './transportOptions'

const { combine, splat, timestamp, simple } = format

const logger = createLogger({
    format: combine(
        splat(),
        timestamp(),
        simple()
    ),
    levels: config.syslog.levels,
    transports: [
        // new transports.File(transportOptions.fileAll),
        new transports.File(transportOptions.fileError),
        new transports.File(transportOptions.fileInfo)
    ],
    exitOnError: false
})

logger.stream = {
    write: function(message, encoding) {
        logger.debug(message)
    }
}

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
    logger.add(new transports.Console(transportOptions.console))
}

export default logger
