import fs from 'fs'
import { format } from 'winston'

const rootDir = fs.realpathSync(process.cwd())

export const transportOptions = {
    fileAll: {
        filename: `${ rootDir }/logs/combined.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880
    },
    fileError: {
        level: 'error',
        filename: `${ rootDir }/logs/error.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880,
        maxFiles: 5,
        colorize: false
    },
    fileInfo: {
        level: 'info',
        filename: `${ rootDir }/logs/info.log`,
        json: true,
        maxsize: 5242880,
        maxFiles: 1,
        colorize: false
    },
    console: {
        level: 'debug',
        filename: `${ rootDir }/logs/debug.log`,
        format: format.simple(),
        handleExceptions: true,
        json: false,
        colorize: true
    }
}
