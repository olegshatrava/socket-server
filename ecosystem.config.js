module.exports = {
	apps: [{
		name: 'notification-socket',
		script: './server.js',
		env: {
			NODE_ENV: 'production'
		}
	}],

	deploy: {
		production: {
			user: 'USER',
			host: 'ip',
			ref: 'origin/master',
			repo: 'repo',
			path: '/home/USER/current',
			'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production'
		}
	}
}
